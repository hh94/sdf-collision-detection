using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MapTextureCreate : MonoBehaviour
{
    // Start is called before the first frame update
    List<Transform> m_Barrier = new List<Transform>();
    public static readonly int[] MapTextureSize = new int[2] { 256, 256 };  //生成的图片 单位像素
    public static readonly float[] MapSize = new float[2] { 10.0f, 10.0f };   //实际地图 单位米
    public Texture2D m_MapTexture;
    //public Texture2D m_MapSDF;
    void Start()
    {
        m_MapTexture = new Texture2D(MapTextureSize[0], MapTextureSize[1]);
        foreach (Transform item in transform)
        {
            m_Barrier.Add(item);
        }
        //CreateColor();
        CreatePolygon();
    }

    List<List<Vector2>> m_PolygonLB = new List<List<Vector2>>();
    //创建地图度变形数据
    private void CreatePolygon()
    {
        int ColorCount = 0;
        Color32[] m_MapColor = new Color32[m_Barrier.Count * 4 + m_Barrier.Count];

        foreach (Transform item in m_Barrier)
        {
            var position = item.position;
            Vector2 mind = new Vector2((position.x) / MapSize[0] * MapTextureSize[0], (position.z) / MapSize[1] * MapTextureSize[1]);
            //在画布上左上角
            var localScale = item.localScale;
            Vector2 leftUp = new Vector2((position.x - localScale.x / 2) / MapSize[0] * MapTextureSize[0], (position.z + localScale.z / 2) / MapSize[1] * MapTextureSize[1]);
            //在画布上左下角的坐标
            Vector2 leftDown = new Vector2((position.x - localScale.x / 2) / MapSize[0] * MapTextureSize[0], (position.z - localScale.z / 2) / MapSize[1] * MapTextureSize[1]);
            //在画布上右上角的坐标
            Vector2 rightUp = new Vector2((position.x + localScale.x / 2) / MapSize[0] * MapTextureSize[1], (position.z + localScale.z / 2) / MapSize[1] * MapTextureSize[1]);
            //在画布上右下角的坐标
            Vector2 rightDown = new Vector2((position.x + localScale.x / 2) / MapSize[0] * MapTextureSize[1], (position.z - localScale.z / 2) / MapSize[1] * MapTextureSize[1]);

            var eulerAngles = item.eulerAngles;
            leftUp = Rotate(mind, leftUp, eulerAngles);
            leftDown = Rotate(mind, leftDown, eulerAngles);
            rightUp = Rotate(mind, rightUp, eulerAngles);
            rightDown = Rotate(mind, rightDown, eulerAngles);

            m_MapColor[ColorCount++] = new Color32(4, 4,0, 1);
            m_MapColor[ColorCount++] = new Color32((byte)leftUp.x , (byte)leftUp.y, 0, 1);
            m_MapColor[ColorCount++] = new Color32((byte)rightUp.x, (byte)rightUp.y, 0, 1);
            m_MapColor[ColorCount++] = new Color32((byte)rightDown.x, (byte)rightDown.y, 0, 1);
            m_MapColor[ColorCount++] = new Color32((byte)leftDown.x, (byte)leftDown.y, 0, 1);
        }
        Texture2D t = new Texture2D(ColorCount, 1);
        t.SetPixels32(m_MapColor);
        t.Apply();
        var bytes = t.EncodeToPNG();
        SaveNativeFile(bytes, Application.dataPath + "/Textures/MapPolygon.jpg");
        Destroy(t);


    }

    //暴力获取地图，其实可以将障碍物的点存起来，效率会快x*y*n倍
    private void CreateColor()
    {
        Color[] mapColor = new Color[MapTextureSize[0] * MapTextureSize[1]];
        for (int y = 0; y < MapTextureSize[1]; y++)
        {
            for (int x = 0; x < MapTextureSize[0]; x++)
            {
                //m_MapColor[i] = Color.white;
                Color color = IsBarrier(x, y) ? Color.black : Color.white;
                mapColor[MapTextureSize[0] * y + x] = color;
            }
        }
        m_MapTexture.SetPixels(mapColor);
        m_MapTexture.Apply();
        var bytes = m_MapTexture.EncodeToJPG();
        SaveNativeFile(bytes, Application.dataPath + "/Textures/Map.jpg");
    }


    private bool IsBarrier(int x, int y)
    {
        List<Vector2> temp = new List<Vector2>();
        foreach (Transform item in m_Barrier)
        {
            temp.Clear();
            var position = item.position;
            Vector2 mind = new Vector2((position.x) / MapSize[0] * MapTextureSize[0], (position.z) / MapSize[1] * MapTextureSize[1]);
            //在画布上左上角
            var localScale = item.localScale;
            Vector2 leftUp = new Vector2((position.x - localScale.x / 2) / MapSize[0] * MapTextureSize[0], (position.z + localScale.z / 2) / MapSize[1] * MapTextureSize[1]);
            //在画布上左下角的坐标
            Vector2 leftDown = new Vector2((position.x - localScale.x / 2) / MapSize[0] * MapTextureSize[0], (position.z - localScale.z / 2) / MapSize[1] * MapTextureSize[1]);
            //在画布上右上角的坐标
            Vector2 rightUp = new Vector2((position.x + localScale.x / 2) / MapSize[0] * MapTextureSize[1], (position.z + localScale.z / 2) / MapSize[1] * MapTextureSize[1]);
            //在画布上右下角的坐标
            Vector2 rightDown = new Vector2((position.x + localScale.x / 2) / MapSize[0] * MapTextureSize[1], (position.z - localScale.z / 2) / MapSize[1] * MapTextureSize[1]);

            var eulerAngles = item.eulerAngles;
            leftUp = Rotate(mind, leftUp, eulerAngles);
            leftDown = Rotate(mind, leftDown, eulerAngles);
            rightUp = Rotate(mind, rightUp, eulerAngles);
            rightDown = Rotate(mind, rightDown, eulerAngles);
            temp.Add(leftUp);
            temp.Add(leftDown);
            temp.Add(rightDown);
            temp.Add(rightUp);
            if (IsInterior(x, y, temp))
            {
                return true;
            }
        }
        return false;
    }



    private Vector2 Rotate(Vector2 midPos, Vector2 rotatePos, Vector3 angle)
    {
        Vector2 dir = rotatePos - midPos;
        Vector3 vs = Quaternion.Euler(angle.x, angle.y, angle.z) * new Vector3(dir.x, 0, dir.y) + new Vector3(midPos.x, 0, midPos.y);
        return new Vector2(vs.x, vs.z);
        //Vector2 dir = rotatePos - midPos;
        //float x = Mathf.Cos(angle.y) * dir.x + Mathf.Sin(angle.y) * dir.y + midPos.x;
        //float y = Mathf.Sin(angle.y) * dir.x - Mathf.Cos(angle.y) * dir.y + midPos.y;
        //Debug.Log(x +" "+y);
        //return new Vector2(x, y);
    }

    private bool IsInterior(int x, int y, List<Vector2> plist)
    {
        int nCross = 0;

        for (int i = 0; i < plist.Count; i++)
        {
            Vector2 p1 = plist[i];
            Vector2 p2 = plist[(i + 1) % plist.Count];
            if (Math.Abs(p1.y - p2.y) < 0.001f)
                continue;
            if (y < Mathf.Min(p1.y, p2.y))
                continue;
            if (y >= Mathf.Max(p1.y, p2.y))
                continue;
            double xx = (double)(y - p1.y) * (double)(p2.x - p1.x) / (double)(p2.y - p1.y) + p1.x;
            if (xx > x)
                nCross++;
        }

        if (nCross % 2 == 1)
        {

            return true;
        }
        else
        {

            return false;
        }
    }

    public static void SaveNativeFile(byte[] bytes, string path)
    {
        if (bytes == null)
            return;
        string directoryPath = path.Substring(0, path.LastIndexOf('/'));
        if (!Directory.Exists(directoryPath))
            Directory.CreateDirectory(directoryPath);
        FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
        fs.Write(bytes, 0, bytes.Length);
        fs.Flush();
        fs.Close();

    }


    // Update is called once per frame
    void Update()
    {

    }
}
