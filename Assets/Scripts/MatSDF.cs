using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MatSDF : MonoBehaviour
{
    // Start is called before the first frame update
    public Texture2D m_MapTexture;    //地图
    Texture2D m_MapSDFTexture;
    List<Vector2> m_ObstacleCoordinates = new List<Vector2>();//障碍物坐标 R G坐标表示xy
    void Start()
    {
        CreateSdf2();
    }

    /**********根据和距离多边形的距离生成的sdf图*************/
    private void CreateSdf2()
    {
        Color32[] colorSDF = m_MapTexture.GetPixels32();
        m_MapSDFTexture = new Texture2D(MapTextureCreate.MapTextureSize[0] + 1, MapTextureCreate.MapTextureSize[1] + 1);
        Color[] color = new Color[(MapTextureCreate.MapTextureSize[0] + 1) * (MapTextureCreate.MapTextureSize[1] + 1)];

        for (int i = 0; i < m_MapSDFTexture.height; i++)
        {
            for (int j = 0; j < m_MapSDFTexture.width; j++)
            {
                float min = comparison(j, i);
                min = (float)Math.Round(min/256.0f, 2);
                color[m_MapSDFTexture.height * i + j] = new Color(min, min, min, 1);
            }
        }
        //写入完成
        m_MapSDFTexture.SetPixels(color);
        m_MapSDFTexture.Apply();
        var bytes = m_MapSDFTexture.EncodeToPNG();
        MapTextureCreate.SaveNativeFile(bytes, Application.dataPath + "/Textures/MapPolygonSDF.jpg");
        Debug.Log("SDF地图生成结束");
        Destroy(m_MapSDFTexture);

        float comparison(int x, int y)
        {
            float min = float.MaxValue;

            for (int i = 0; i < colorSDF.Length;)
            {
                int polygonCount = colorSDF[i].r;
                bool isInterior = false;
                for (int j = 0; j < polygonCount; j++)
                {
                    //如果生成的地图大于256则不能不用颜色的方式进行保存。
                    Vector2 point1 = new Vector2(colorSDF[i + j + 1].r, colorSDF[i + j + 1].g);
                    int next = (j + 2) > polygonCount ? i + 1 : i + j + 2;
                    Vector2 point2 = new Vector2(colorSDF[next].r, colorSDF[next].g);
                    float tempMin = PolygonDistance(point1, point2, new Vector2(x, y), ref isInterior);
                    if (tempMin < min)
                    {
                        min = tempMin;
                    }
                }
                //如果在多边形内部则为零
                if (isInterior)
                {
                    min = 0;
                }

                i += (polygonCount + 1);
            }
            return min;
        }
    }

    private float PolygonDistance(Vector2 ppolygonp1, Vector2 ppolygonp2, Vector2 pdstpoint, ref bool interior)
    {

        if ((((ppolygonp1.y > pdstpoint.y) != (ppolygonp2.y > pdstpoint.y))) && (
            (pdstpoint.x < (ppolygonp2.x - ppolygonp1.x) * (pdstpoint.y - ppolygonp1.y) / (ppolygonp2.y - ppolygonp1.y) + ppolygonp1.x))
            )
            interior = !interior;


        Vector2 vpolygon = ppolygonp2 - ppolygonp1;
        Vector2 vdespoint = pdstpoint - ppolygonp1;

        float min = float.MaxValue;
        float polygonlentht = vpolygon.magnitude * vpolygon.magnitude;
        float t = Vector2.Dot(vpolygon, vdespoint) / polygonlentht;
        if (t < 0)
        {
            min = vdespoint.magnitude;
        }
        else if (t >= 0 && t <= 1)
        {
            Vector2 da = (vpolygon) * t + ppolygonp1;
            min = (pdstpoint - da).magnitude;
        }
        else
        {
            min = (pdstpoint - ppolygonp2).magnitude;
        }
        return min;
    }


    /**********根据最近的顶点生成的sdf图每个点的数据是和最近顶点的距离*************/
    private void CreateSdf()
    {
        Color[] colorSDF = new Color[(m_MapTexture.width + 1) * (m_MapTexture.height + 1)];
        m_MapSDFTexture = new Texture2D(m_MapTexture.width + 1, m_MapTexture.height + 1);
        Color[] pix = m_MapTexture.GetPixels();
        for (int y = 0; y < m_MapTexture.height; y++)
        {
            for (int x = 0; x < m_MapTexture.width; x++)
            {
                //获得所有障碍物区
                if (pix[y * m_MapTexture.width + x].r <= 0.5f)
                {
                    //将XY坐标记录在颜色区RG。
                    Vector2 temp = new Vector2(x, y);
                    m_ObstacleCoordinates.Add(temp);
                }
            }
        }
        /*********************/
        for (int y = 0; y < m_MapSDFTexture.height; y++)
        {
            for (int x = 0; x < m_MapSDFTexture.width; x++)
            {
                float sqrdiance = Find(x, y);
                sqrdiance = Mathf.Sqrt(sqrdiance);
                colorSDF[y * m_MapSDFTexture.width + x] = new Color(sqrdiance / 256.0f, sqrdiance / 256.0f, sqrdiance / 256.0f);
            }
        }
        m_MapSDFTexture.SetPixels(colorSDF);
        var bytes = m_MapSDFTexture.EncodeToPNG();
        MapTextureCreate.SaveNativeFile(bytes, Application.dataPath + "/Textures/MapSDF.jpg");
        Debug.Log("SDF地图生成结束");
    }

    //找到最近的点  最近的点的比较方法就是x点和y点相聚最近的点的某一点
    private float Find(int x, int y)
    {
        float minSqr = float.MaxValue;
        float sqrdis = 0;
        for (int i = 0; i < m_ObstacleCoordinates.Count; i++)
        {
            Vector2 temp = m_ObstacleCoordinates[i];
            //if (x == temp.x && y == temp.y) {
            //    sqrdis = 256*256;
            //}
            if (temp.x >= x && temp.y >= y)
            {
                sqrdis = Vector2.SqrMagnitude(new Vector2(temp.x, temp.y) - new Vector2(x, y));
            }
            else if (temp.x >= x && temp.y < y)
            {
                sqrdis = Vector2.SqrMagnitude(new Vector2(temp.x, temp.y + 1) - new Vector2(x, y));
            }
            else if (temp.x < x && temp.y >= y)
            {
                sqrdis = Vector2.SqrMagnitude(new Vector2(temp.x + 1, temp.y) - new Vector2(x, y));
            }
            else if (temp.x < x && temp.y < y)
            {
                sqrdis = Vector2.SqrMagnitude(new Vector2(temp.x + 1, temp.y + 1) - new Vector2(x, y));
            }

            if (sqrdis < minSqr)
            {
                minSqr = sqrdis;
            }
        }
        return minSqr;
    }

    /***********************/

}
