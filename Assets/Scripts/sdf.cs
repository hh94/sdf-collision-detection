using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sdf : MonoBehaviour
{
    public Texture2D a;
    public Texture2D B;
    private void Start()
    {
        B = Generator(a);
        var bytes = B.EncodeToJPG();
        MapTextureCreate.SaveNativeFile(bytes, Application.dataPath + "/Textures/Map.jpg");
    }

    public struct Pixel
    {
        public float distance;
        public bool edge;
    }

    public int targetSize = 64;
    private int m_x_dims;
    private int m_y_dims;
    public Pixel[] m_pixels;


    public void LoadFromTexture(Texture2D texture)
    {
        Color[] texpixels = texture.GetPixels();
        m_x_dims = texture.width;
        m_y_dims = texture.height;
        m_pixels = new Pixel[m_x_dims * m_y_dims];
        for (int i = 0; i < m_pixels.Length; i++)
        {
            if (texpixels[i].r > 0.5f)
                m_pixels[i].distance = -99999f;
            else
                m_pixels[i].distance = 99999f;
        }
    }

    void BuildSweepGrids(out float[] outside_grid, out float[] inside_grid)
    {
        outside_grid = new float[m_pixels.Length];
        inside_grid = new float[m_pixels.Length];
        for (int i = 0; i < m_pixels.Length; i++)
        {
            if (m_pixels[i].distance < 0)   //��ɫ����
            {
                //inside pixel. outer distance is set to 0, inner distance
                //is preserved (albeit negated to make it positive)
                outside_grid[i] = 0f;
                inside_grid[i] = -m_pixels[i].distance;
            }
            else
            {
                //outside pixel. inner distance is set to 0,
                //outer distance is preserved
                inside_grid[i] = 0f;
                outside_grid[i] = m_pixels[i].distance;
            }
        }
    }

    public Texture2D Generator(Texture2D source)
    {
        LoadFromTexture(source);

        ClearAndMarkNoneEdgePixels();
        float[] outside_grid, inside_grid;
        BuildSweepGrids(out outside_grid, out inside_grid);

        //run the 8PSSEDT sweep on each grid
        SweepGrid(outside_grid);
        SweepGrid(inside_grid);


        var pixels = new Color32[targetSize * targetSize];
        int scaleX = source.width / targetSize;
        int scaleY = source.height / targetSize;

        for (int y = 0; y < targetSize; y++)
        {
            for (int x = 0; x < targetSize; x++)
            {
                int i = y * targetSize + x;
                float dist1 = inside_grid[y * scaleY * m_x_dims + x * scaleX];
                float dist2 = outside_grid[y * scaleY * m_x_dims + x * scaleX];
                var dist = Mathf.Clamp(128 + (dist1 - dist2), 0, 255);
                pixels[i] = new Color32((byte)dist, (byte)dist, (byte)dist, 255);
            }
        }

        Texture2D dest = new Texture2D(targetSize, targetSize);
        dest.SetPixels32(pixels);
        dest.Apply();

        return dest;
    }




    //compares a pixel for the sweep, and updates it with a new distance if necessary
    public void Compare(float[] grid, int x, int y, int xoffset, int yoffset)
    {
        //calculate the location of the other pixel, and bail if in valid
        int otherx = x + xoffset;
        int othery = y + yoffset;
        if (otherx < 0 || othery < 0 || otherx >= m_x_dims || othery >= m_y_dims)
            return;

        //read the distance values stored in both this and the other pixel
        float curr_dist = grid[y * m_x_dims + x];
        float other_dist = grid[othery * m_x_dims + otherx];

        //calculate a potential new distance, using the one stored in the other pixel,
        //PLUS the distance to the other pixel
        float new_dist = other_dist + Mathf.Sqrt(xoffset * xoffset + yoffset * yoffset);

        //if the potential new distance is better than our current one, update!
        if (new_dist < curr_dist)
            grid[y * m_x_dims + x] = new_dist;
    }

    public void SweepGrid(float[] grid)
    {
        // Pass 0
        //loop over rows from top to bottom
        for (int y = 0; y < m_y_dims; y++)
        {
            //loop over pixels from left to right
            for (int x = 0; x < m_x_dims; x++)
            {
                Compare(grid, x, y, -1, 0);
                Compare(grid, x, y, 0, -1);
                Compare(grid, x, y, -1, -1);
                Compare(grid, x, y, 1, -1);
            }

            //loop over pixels from right to left
            for (int x = m_x_dims - 1; x >= 0; x--)
            {
                Compare(grid, x, y, 1, 0);
            }
        }

        // Pass 1
        //loop over rows from bottom to top
        for (int y = m_y_dims - 1; y >= 0; y--)
        {
            //loop over pixels from right to left
            for (int x = m_x_dims - 1; x >= 0; x--)
            {
                Compare(grid, x, y, 1, 0);
                Compare(grid, x, y, 0, 1);
                Compare(grid, x, y, -1, 1);
                Compare(grid, x, y, 1, 1);
            }

            //loop over pixels from left to right
            for (int x = 0; x < m_x_dims; x++)
            {
                Compare(grid, x, y, -1, 0);
            }
        }
    }



    Pixel GetPixel(int x, int y)
    {
        return m_pixels[y * m_x_dims + x];
    }
    void SetPixel(int x, int y, Pixel p)
    {
        m_pixels[y * m_x_dims + x] = p;
    }

    bool IsOuterPixel(int pix_x, int pix_y)
    {
        if (pix_x < 0 || pix_y < 0 || pix_x >= m_x_dims || pix_y >= m_y_dims)
            return true;
        else
            return GetPixel(pix_x, pix_y).distance >= 0;
    }

    bool IsEdgePixel(int pix_x, int pix_y)
    {
        bool is_outer = IsOuterPixel(pix_x, pix_y);
        if (is_outer != IsOuterPixel(pix_x - 1, pix_y - 1)) return true; //[-1,-1]
        if (is_outer != IsOuterPixel(pix_x, pix_y - 1)) return true;     //[ 0,-1]
        if (is_outer != IsOuterPixel(pix_x + 1, pix_y - 1)) return true; //[+1,-1]
        if (is_outer != IsOuterPixel(pix_x - 1, pix_y)) return true;     //[-1, 0]
        if (is_outer != IsOuterPixel(pix_x + 1, pix_y)) return true;     //[+1, 0]
        if (is_outer != IsOuterPixel(pix_x - 1, pix_y + 1)) return true; //[-1,+1]
        if (is_outer != IsOuterPixel(pix_x, pix_y + 1)) return true;     //[ 0,+1]
        if (is_outer != IsOuterPixel(pix_x + 1, pix_y + 1)) return true; //[+1,+1]
        return false;
    }

    public void ClearAndMarkNoneEdgePixels()
    {
        for (int y = 0; y < m_y_dims; y++)
        {
            for (int x = 0; x < m_y_dims; x++)
            {
                Pixel pix = GetPixel(x, y);
                pix.edge = IsEdgePixel(x, y); //for eikonal sweep, mark edge pixels
                if (!pix.edge)
                    pix.distance = pix.distance > 0 ? 99999f : -99999f;
                SetPixel(x, y, pix);
            }
        }
    }
}
