using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMin : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform player;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float minxf = float.MaxValue;
        float minzf = float.MaxValue;
        Transform minx = null;
        Transform minz = null;
        foreach (Transform item in transform)
        {
            item.GetComponent<MeshRenderer>().materials[0].SetColor("_Color", Color.black);
            if (Mathf.Abs(item.position.x-player.position.x) < minxf)
            {
                minxf = Mathf.Abs(item.position.x - player.position.x);
                minx = item;
            }

            if (Mathf.Abs(item.position.z - player.position.z) < minzf)
            {
                minzf = Mathf.Abs(item.position.z - player.position.z);
                minz = item;
            }
        }
        float v1 = Vector3.Distance(player.position, minx.position);

        float v2 = Vector3.Distance(player.position, minz.position);
        if (v1 > v2)
        {
            minz.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.red);
        }
        else {
            minx.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.red);
        }
    }
}
